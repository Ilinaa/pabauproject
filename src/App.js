import './App.css';
import Header from './Components/Header/Header';
import Content from './Components/Content/Content';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import Footer from './Components/Footer/Footer';

const App = () => {
  return (
    <Layout>
      <Header />
      <Content />
      <Footer/>
    </Layout>
  )
}

export default App;

