import { Card } from 'antd';
import Meta from 'antd/lib/card/Meta';
import React from 'react';
import './TopCard.css';

const TopCard = ({icon,title,handleFilter,active,setActive,selectedSubCategory}) => {

    return ( 
        <Card
          hoverable
          cover={icon}
          className={`top_card ${active && active.mainCategory === title && "active"}`}
          onClick={()=> {handleFilter(title); setActive({ mainCategory: title, category: selectedSubCategory && selectedSubCategory, subCategory: "In Clinic" })}}
        >
        <Meta title={title}/>
       </Card>
     );
}
 
export default TopCard;