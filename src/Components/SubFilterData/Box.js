import { CheckCircleOutlined, ClockCircleOutlined, QuestionCircleOutlined, StarFilled } from '@ant-design/icons';
import { Alert, Button, Card, Modal } from 'antd';
import React from 'react';
import styled from 'styled-components';
import ContentData from '../../ContentData/ContentData';
import usePopUp from '../Hooks/usePopUp';

const CardWrapper = styled(Card)`
    width: 100%;
    padding: 16px !important;
    border-radius: 4px;
    margin: 16px 0px!important;
    .box_middle,.box_bottom{
        text-align: left !important;
        padding: 7px 0px;
    }
    .box_top{
        display: flex;
        justify-content: space-between;
    }
    .box_top .box_span_name{
        font-size: 20px;
        line-height: 24px;
        color: #3d3d46;
        margin-right: 5px;
        text-transform: capitalize;
    }
    .box_top .box_span_right{
        font-weight: 500;
        font-size: 16px;
        line-height: 24px;
        color: #65cd98;
    }
    .box_span_reviews{
        margin-left: 10px;
        color: #737387;
        font-size: 16px;
    }
    .box_middle span{
        color: #737387;
        font-size: 16px;
        line-height: 20px;
    }
    .box_selected{
        text-align: right;
    }
    .box_selected button{
        background-color: #54b2d3 !important;
        border-radius: 4px !important;
        color: #fff !important;
        font-size: 14px;
        font-weight: 700;
    }

`

const Selected = ({ isSelectedList, button, name, handlePopUp }) => {
    return (
        isSelectedList.map((el, i) => (
            el === name ?
                button ?
                    <Button className="selected_button" icon={<CheckCircleOutlined key={i} />}>{ContentData.Selected.box_button} </Button> :
                    <Alert
                        banner
                        message={ContentData.SubFilterData.selectedMessage}
                        key={i}
                        type="info"
                        action={
                            <Button size="small" type="text" onClick={() => handlePopUp()}>
                                {ContentData.SubFilterData.moreInfo}
                            </Button>
                        } />
                : null
        ))
    )
}
const Box = ({ name, price, time, review, handleSelected, isSelectedList }) => {
    const [popUp, handlePopUp] = usePopUp();
    return (
        <>
            <CardWrapper
                hoverable
                // className="box"
                name={name}
                onClick={() => handleSelected(name, price)}
            >
                <div className="box_top">
                    <div>
                        <span className="box_span_name">{name} </span>
                        <span><QuestionCircleOutlined /></span>
                    </div>
                    <span className="box_span_right">&#163; {price}</span>
                </div>
                <div className="box_middle">
                    <span>{time} min <span><ClockCircleOutlined /></span></span>
                </div>
                <div className="box_bottom">
                    {([...Array(5)].map((x, i) => <StarFilled key={i} style={{ color: "#fadb14", fontSize: "20px" }} />))}
                    <span className="box_span_reviews">{review} reviews</span>
                </div>
                <div className="box_selected">
                    <Selected isSelectedList={isSelectedList} button handlePopUp={handlePopUp} name={name} />
                </div>

            </CardWrapper>
            <Selected isSelectedList={isSelectedList} handlePopUp={handlePopUp} name={name} />
            <Modal title={ContentData.MoreInfoModal.heading} visible={popUp} onCancel={handlePopUp}
                footer={[
                    <Button key="back" onClick={handlePopUp} type="primary">
                        I understand
                    </Button>
                ]}
            >
                <p>{ContentData.MoreInfoModal.text}</p>
            </Modal>
        </>
    );
}

export default Box;