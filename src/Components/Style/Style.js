import styled from 'styled-components';
import { List } from 'antd';


export const BoxStyle = styled.div`
    max_height:${props => props.scroll && "605px"};
    overflow-y:${props => props.scroll && "auto"};
    background: #fff;
    padding: 20px; 
    margin-right: 16px;
    margin-top:${props => props.marginTop && "10px"};
    box-sizing: border-box;
    .phone{
        color:#54b2d3;
        margin-left:5px;
    }
`
