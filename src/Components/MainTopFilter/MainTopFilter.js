import { Col, Row } from 'antd';
import React, { useEffect } from 'react';
import { defaultItems } from '../../data';
import { AppstoreOutlined, CreditCardOutlined } from '@ant-design/icons';
import TopCard from '../TopCard/TopCard';
import useFilter from '../Hooks/useFilter';
import { BoxStyle } from '../Style/Style';



const MainTopFilter = ({setmainFilter,active, setActive}) => {

   const [filtered, handleFilter] = useFilter(defaultItems)

   useEffect(()=>{
      setmainFilter(filtered)
   },[filtered])

   return ( 
        <BoxStyle>
            <Row>
               <Col span={3} >
                  <TopCard handleFilter={handleFilter} title="All" setActive={setActive} active={active} selectedSubCategory={"Botox"} icon={<AppstoreOutlined style={{ fontSize: '40px',color:"rgb(84, 178, 211)"}} />}/>
               </Col>
               {defaultItems.map((el,index)=>(
                   <Col span={3} key={index}>
                      <TopCard title={el.name} icon={<el.icon/>} handleFilter={handleFilter} setActive={setActive} active={active} selectedSubCategory={el.category[0].name}/>
                   </Col>
                   ))
               }
              <Col span={3}>
                 <TopCard handleFilter={handleFilter} setActive={setActive} active={active}  title="Voucher" icon={<CreditCardOutlined style={{ fontSize: '40px' ,color:"rgb(255, 203, 100)"}}/>}/>
              </Col>
            </Row>
         </BoxStyle>
     );
}
 
export default MainTopFilter;