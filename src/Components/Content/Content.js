import React, { useState } from 'react';
import MainTopFilter from '../MainTopFilter/MainTopFilter';
import { Row, Col, Button } from 'antd';
import SubFilter from '../SubFilter/SubFilter';
import SubFilterData from '../SubFilterData/SubFilterData';
import { List } from 'antd';
import ContentData from '../../ContentData/ContentData';
import { useEffect } from 'react/cjs/react.development';
import { ArrowRightOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import { BoxStyle } from '../Style/Style'

const ContentWrapper = styled.div`
  background: #f7f7f9;
  padding: 50px 0px;
  .content_bottom{
    margin: 32px 0px 100px;
  }
  .fixed_top{
    width: 100%;
    position: fixed !important;
    bottom: 45px;
    .price_popUp{
      border-radius: 4px 4px 0px 0px;
      display: flex !important;
      align-items: center !important;
      background: #fff;
      margin-right: 16px !important;
      justify-content: space-between !important;
      padding: 16px 20px;
      box-shadow: 0 0 3px 0 rgba(0,0,0,.2784313725490196),0 0 0 1px rgba(0,0,0,.0784313725490196)!important;
      p, .button{
        text-align: right !important;
        margin: 0px;

      }
      p{
        color: #54b2d3;
        font-size: 20px;
        font-weight: 500;
    
      }

    }
  }
`
const PricePopUp = styled.div`
    border-radius: 4px 4px 0px 0px;
    display: flex !important;
    align-items: center !important;
    background: #fff;
    justify-content: space-between !important;
    padding: 16px 20px;
    box-shadow: 0 0 3px 0 rgba(0,0,0,.2784313725490196),0 0 0 1px rgba(0,0,0,.0784313725490196)!important;
    margin-right: 16px !important;
    .p , button{
      text-align: right !important;
      margin: 0px;
    }
    p{
      color: #54b2d3;
      font-size: 20px;
      font-weight: 500;
    }
`


const Content = () => {
  const [mainFilter, setmainFilter] = useState();
  const [filterdSubCategory, setFilteredSubCategory] = useState()
  const [active, setActive] = useState({ mainCategory: "All", category: "Botox", subCategory: "In Clinic" })
  const [status, setStatus] = useState("In Clinic")
  const [checkOnline, setCheckOnline] = useState(false);
  const [isSelectedList, setisSelectedList] = useState([]);
  const [showPrice, setshowPrice] = useState(0)

  const handleSelected = (name, price) => {
    let actualPrice = parseInt(price.split("-")[0]);
    let selectedItems = [...isSelectedList];

    // Checks if the name is in the list, if it is, then finds it and remove it, or add it to list

    if (selectedItems.includes(name)) {
      let index = selectedItems.indexOf(name)
      if (index !== -1) {
        selectedItems.splice(index, 1)
        setshowPrice(showPrice - actualPrice)
      }
    } else {
      selectedItems.push(name)
      setshowPrice(actualPrice + showPrice)
    }
    setisSelectedList(selectedItems)
  }

  useEffect(() => {
    setStatus("In Clinic")
  }, [filterdSubCategory])

  return (
    <ContentWrapper>
      <Row>
        <Col span={12} offset={6}>
          <MainTopFilter
            setmainFilter={setmainFilter}
            active={active}
            setActive={setActive}
          />
        </Col>
      </Row>
      <Row className="content_bottom">
        <Col span={6} offset={6} >
          <BoxStyle scroll>
            {mainFilter && mainFilter.map((filterItem, id) =>
              <SubFilter
                key={id}
                category={filterItem.category}
                setFilteredSubCategory={setFilteredSubCategory}
                active={active}
                setActive={setActive}
              />
            )}
          </BoxStyle>
        </Col>
        <Col span={6}>
          <BoxStyle>
            <List
              dataSource={checkOnline ? [ContentData.SubCategoryChoices[0]] : ContentData.SubCategoryChoices}
              className="box_list"
              renderItem={item => (
                <List.Item className={`list_style ${checkOnline && "max_width"} ${status === item.name && "active"}`} onClick={() => setStatus(item.name)}>
                  <span><item.icon /></span> {item.name}
                </List.Item>
              )}
            />
            {filterdSubCategory && filterdSubCategory.map((subCategory, id) => (
              subCategory.subCategory.map((el, index) => (
                <SubFilterData
                  subCategory={el}
                  status={status}
                  key={index}
                  setCheckOnline={setCheckOnline}
                  handleSelected={(name, price) => handleSelected(name, price)}
                  isSelectedList={isSelectedList}
                />
              ))
            ))}
          </BoxStyle>
          <Row>
            <Col span={24}>
              <BoxStyle marginTop> 
                 <p>{ContentData.Contact.text} <span className="phone">{ContentData.Contact.phone}</span></p> 
              </BoxStyle>
            </Col>
          </Row> 
        </Col>
      </Row>
      {showPrice > 0 &&
        <Row className="fixed_top">
          <Col span={12} offset={6}>
            <Row className="price_popUp">
              <Col span={14}>
                <p> {isSelectedList.length} {ContentData.Selected.span} &#163; {showPrice}</p>
              </Col>
              <Col span={10} className="button">
                <Button type="primary">{ContentData.Selected.button} <ArrowRightOutlined /></Button>
              </Col>
            </Row>
          </Col>
        </Row>
      }
    </ContentWrapper >
  );
}

export default Content;
