import { Typography } from 'antd';
import React from 'react';
import ContentData from '../../ContentData/ContentData';
import './Header.css';

const { Title ,Text} = Typography;

const Header = () => {
    return (
        <div className="header_wrapper">
         <Title level={5}>{ContentData.Header.text}</Title>
         <Text>{ContentData.Header.span}</Text>
        </div>
    );
}
 
export default Header;